import discord
from discord.ext import commands

import os
import datetime
import json
import pandas as pd
import pandas_datareader as pdr
import plotly.graph_objects as go

from dateutil.relativedelta import relativedelta

def setup(client):
    client.add_cog(Finance(client))

class Finance(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.imgdir = os.path.join(os.getcwd(), 'img/finance/')
        self.crypto = {}
        self.currencies = {}
        self.loadCrypto()
        self.loadCurrencies()

    ################## COMMANDS ##################
    @commands.command()
    async def cryptograph(self, ctx, crypto='bitcoin', start='1year', end='today', currency='USD'):
      start = self.getDate(start)
      end = self.getDate(end)
      crypto = self.getCrypto(crypto.lower())
      currency_symbol = self.getCurrency(currency.upper())

      df = pdr.DataReader(f'{crypto}-{currency}', 'yahoo', start, end)
      fig = go.Figure(
        data = [
            go.Candlestick(
                x = df.index,
                open = df.Open,
                high = df.High,
                low = df.Low,
                close = df.Close
            ),
        ]
      )
      fig.update_layout(
              title = f'Candlestick graph for {crypto} - source: finance.yahoo.com',
              xaxis_title = 'Date',
              yaxis_title = f'Price ({currency})',    xaxis_rangeslider_visible = False
      )
      fig.update_yaxes(tickprefix=f'{currency_symbol}')

      if fig != None:
        imgpath = os.path.join(self.imgdir, f'{crypto}{currency}{start}{end}.png')
        fig.write_image(imgpath)

        with open(imgpath, "rb") as fh:
          img = discord.File(fh, filename=f'{crypto}{currency}{start}{end}.png')
        await ctx.send(file=img)
    
    @commands.command()
    async def bitcoin(self, ctx, start='1year', end='now', currency='USD'):
      await self.cryptograph(ctx, crypto='bitcoin', currency=currency, start=start, end=end)
    
    @commands.command()
    async def ethereum(self, ctx, start='1year', end='now', currency='USD'):
      await self.cryptograph(ctx, crypto='ethereum', start=start, end=end, currency=currency)
    
    @commands.command()
    async def binance(self, ctx, start='1year', end='now', currency='USD'):
      await self.cryptograph(ctx, crypto='binancecoin', start=start, end=end, currency=currency)

    ################## FUNCTIONS ##################
    def loadCrypto(self):
        with open(os.path.join('data', 'cryptocurrencies.json'), 'r') as f:
            self.crypto = json.load(f)

    def loadCurrencies(self):
      with open(os.path.join('data', 'currencies.json'), 'r') as f:
        self.currencies = json.load(f)

    def getCrypto(self, name):
      return self.crypto.get(name, 'BTC')
    
    def getCurrency(self, name):
      return self.currencies.get(name, 'USD')


    def getDate(self, keyword):
      ndate = datetime.datetime.now()
      rdate = ndate
      if keyword=='now':
        rdate = ndate
      elif keyword=='today':
        rdate = ndate
      elif keyword=='yesterday':
        rdate = rdate.replace(day=rdate.day-1)
      elif keyword.endswith('year'):
        try:
          n = int(keyword.replace('year', ''))
        except ValueError:
          n = 1
        rdate = rdate - relativedelta(years=n)
      elif keyword.endswith('y'):
        try:
          n = int(keyword.replace('y', ''))
        except ValueError:
          n = 1
        rdate = rdate - relativedelta(years=n)
      elif keyword.endswith('month'):
        try:
          n = int(keyword.replace('month', ''))
        except ValueError:
          n = 1
        rdate = rdate - relativedelta(months=n)
      elif keyword.endswith('m'):
        try:
          n = int(keyword.replace('m', ''))
        except ValueError:
          n = 1
        rdate = rdate - relativedelta(months=n)
      elif keyword.endswith('day'):
        try:
          n = int(keyword.replace('day', ''))
        except ValueError:
          n = 1
        rdate = rdate - relativedelta(days=n)
      elif keyword.endswith('d'):
        try:
          n = int(keyword.replace('d', ''))
        except ValueError:
          n = 1
        rdate = rdate - relativedelta(days=n)

      return rdate