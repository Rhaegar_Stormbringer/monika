import discord
from discord.ext import commands

import os

def setup(client):
    client.add_cog(Meme(client))

class Meme(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.imgdir = os.path.join(os.getcwd(), 'img/')

    @commands.command()
    async def cringe(self, ctx):
      imgpath = None
      for root, dirs, files in os.walk(self.imgdir):
        if 'cringe.png' in files:
          imgpath = os.path.join(self.imgdir, 'cringe.png')
      if imgpath != None:
        with open(imgpath, "rb") as fh:
          img = discord.File(fh, filename='cringe.png')
        await ctx.send(file=img)