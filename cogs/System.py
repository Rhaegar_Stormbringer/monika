import discord
from discord.ext import commands

import random
import os
import json

def setup(client):
    client.add_cog(System(client))

def getPrefix(client, message):
    with open(os.path.join('data', 'prefixes.json'), 'r') as f:
        prefixes = json.load(f)

    return prefixes.get(str(message.guild.id), '.')

class System(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command(brief='Displays all the command categories and their status')
    async def modules(self, ctx):
      embed=discord.Embed(title="Available Modules", description=f"Use **{getPrefix(self.client, ctx.message)}load** and **{getPrefix(self.client, ctx.message)}unload** to enable / disable these modules and their commands.")
      for filename in os.listdir('./cogs'):
        if filename.endswith('.py'):
          filename = filename[:-3]
          try:
            self.client.load_extension(f'cogs.{filename}')
          except commands.ExtensionAlreadyLoaded:
            embed.add_field(name=filename, value="**Loaded**", inline=True)
          except commands.ExtensionNotFound:
            pass
          except commands.NoEntryPointError:
            pass
          else:
            self.client.unload_extension(f"cogs.{filename}")
            embed.add_field(name=filename, value="Unloaded", inline=True)
      await ctx.send(embed=embed)
      
    @commands.command(brief='Shows delays to the system')
    async def ping(self, ctx) :
      await ctx.send(f"Pong with {str(round(self.client.latency, 2))}s")

    @commands.command(brief='Changes the bot prefix on this server')
    async def prefix(self, ctx, prefix):
      with open(os.path.join('data', 'prefixes.json'), 'r') as f:
        prefixes = json.load(f)

      prefixes[str(ctx.guild.id)] = str(prefix)
      await ctx.send(f"Prefix changed to: {prefix}")

      with open(os.path.join('data', 'prefixes.json'), 'w') as f:
        json.dump(prefixes, f, indent=4)
    
    @commands.command(brief='Deletes X messages from the channel')
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, amount=3) :
      await ctx.channel.purge(limit=amount+1)