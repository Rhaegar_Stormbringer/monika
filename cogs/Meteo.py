import discord
from discord.ext import commands

import random
import os
import json

import pyowm

def setup(client):
    client.add_cog(Meteo(client))

class Meteo(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.owm = pyowm.OWM(os.environ['OWM_API_KEY'])
    
    @commands.command()
    async def weather(self, ctx, city='Toulouse', country='FR'):
      weather = self.owm.weather_manager().weather_at_place(f'{city},{country}').weather
      temp = weather.temperature('celsius')
      wind = weather.wind()
      rain = weather.rain
      pressure = weather.pressure
      icon_url = weather.weather_icon_url()
      print(f"temp : {temp}\nwind : {wind}\nrain : {rain}\npressure : {pressure}\nicon_url : {icon_url}")