import discord
from discord.ext import commands

import random
import os
import json

def setup(client):
    client.add_cog(Game(client))

class Game(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.gamedata = {}

        self.loadingGameData()
        if (self.gamedata != {}):
            print("game data successfully loaded")
        else:
            print("WARNING: game data couldn't loads correctly")

    def loadingGameData(self):
        with open(os.path.join('data', 'gamedata.json'), 'r') as f:
            self.gamedata = json.load(f)

    def getGameData(self, gamename, data='outcomes'):
        return self.gamedata.get(gamename, None).get(data, None)

    @commands.command()
    async def flip(self, ctx):
        await ctx.send(f'{random.choice(self.getGameData(gamename="coin_clipping"))}!')

    @commands.command()
    async def roll(self, ctx):
        roll = random.randint(1, 100)
        response = f"{ctx.message.author.name}, you rolled a {roll}"
        if roll == 100:
            response += f"\ngg {ctx.message.author.name}!"
        await ctx.send(response)

    @commands.command(name="8ball")
    async def eightball(self, ctx, *, question):
        await ctx.send(f"{random.choice(self.getGameData(gamename='8ball'))}")
