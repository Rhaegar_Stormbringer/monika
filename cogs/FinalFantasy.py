import discord
from discord.ext import commands

import pyxivapi

import os

def setup(client):
    client.add_cog(FinalFantasy(client))

class FinalFantasy(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.ffclient = pyxivapi.XIVAPIClient(api_key=os.environ['FF14_API_KEY'])

    ################## COMMANDS ##################
    @commands.command()
    async def fftestchar(self, ctx, world, forename, surname):
      character = await self.ffclient.character_search(
        world=world,
        forename=forename, 
        surname=surname
      )
      print(character)