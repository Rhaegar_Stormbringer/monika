import discord
from discord.ext import commands

import random
import os
import json
import requests

def setup(client):
    client.add_cog(Request(client))

class Request(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command()
    async def inspire(self, ctx):
      response = requests.get("https://zenquotes.io/api/random")
      data = json.loads(response.text)
      quote = f"*{data[0]['q']}* - {data[0]['a']}"
      await ctx.send(quote)
