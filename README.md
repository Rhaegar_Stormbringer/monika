# Monika

Discord bot in Python using the [discord.py API](https://discordpy.readthedocs.io/en/latest/index.html).
The source code for the first version of the bot was made in Java with the jda and jda-utils APIs, and is still available on its [own branch](https://gitlab.com/Rhaegar_Stormbringer/monika/-/tree/java_version).

The bot is currently deployed and developped using the Repl.it platform, as such it's possible that the code presented here is late compared to the [deployed version](https://repl.it/@RhaegarStorm/MonikaDiscordBot).
