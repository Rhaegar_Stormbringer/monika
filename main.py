import discord
from discord.ext import commands

from replit import db

import os
import json
import random

from loopServer import keep_alive

keep_alive()

def getPrefix(client, message):
    with open(os.path.join('data', 'prefixes.json'), 'r') as f:
        prefixes = json.load(f)

    return prefixes.get(str(message.guild.id), '.')

client = commands.Bot(command_prefix= getPrefix)
token = os.environ['CLIENT_TOKEN']

@client.event
async def on_ready() :
    await client.change_presence(status = discord.Status.online)#, activity = discord.Game("Listening to .help"))
    print("I am online")
    print("Loading System cog...")
    client.load_extension(f'cogs.System')
    print("Done")

@client.command()
async def load(ctx, extension, verbose=True):
  try:
    client.load_extension(f'cogs.{extension}')
    if verbose:
      await ctx.send(f"Module **{extension}** successfully loaded")
  except Exception as e:
    print(e)
    if verbose:
      await ctx.send(f"Error while loading module **{extension}**, please contact support")

@client.command()
async def unload(ctx, extension, verbose=True):
  try:
    client.unload_extension(f'cogs.{extension}')
    if verbose:
      await ctx.send(f"Module **{extension}** successfully unloaded")
  except Exception as e:
    print(e)
    if verbose:
      await ctx.send(f"Error while unloading module **{extension}**, please contact support")

@client.command()
async def reload(ctx, extension, verbose=True):
  await unload(ctx, extension, verbose=False)
  await load(ctx, extension, verbose=False)
  if verbose:
    await ctx.send(f"Module **{extension}** successfully reloaded")
    
client.run(token)